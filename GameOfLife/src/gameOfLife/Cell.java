package gameOfLife;

import java.util.ArrayList;

import static gameOfLife.StateOfCell.dead;
import static gameOfLife.StateOfCell.live;

public class Cell {

  //represents all characteristics of cells in the board

  private int row, column, numberOfAliveNeighbours = 0;
  private ArrayList<int[]> aliveNeighbours;
  private StateOfCell state;

  public Cell(int row, int column, StateOfCell state) {

    this.row = row;
    this.column = column;
    this.state = state;
  }

  public void findAliveNeighbours(ArrayList<Cell> cells) {

    for (int[] neighbours : aliveNeighbours)
      for (Cell currentCell : cells) {
        if (currentCell.row == neighbours[0] && currentCell.column == neighbours[1] && currentCell.state == live)
          numberOfAliveNeighbours++;
      }
  }

  public void computeFinalGeneration(ArrayList<int []>nextGeneration) {

    if (state == live && (numberOfAliveNeighbours == 2 || numberOfAliveNeighbours == 3))
      nextGeneration.add(new int[]{row, column});
    if (state == dead && numberOfAliveNeighbours == 3)
      nextGeneration.add(new int[]{row,column});
  }

  public void findNeighbours() {

    ArrayList<int[]> myList = new ArrayList<>();
    for (int possibleRow : new int[]{row - 1, row, row + 1})
      for (int possibleColumn : new int[]{column - 1, column, column + 1}) {
        if ((possibleRow >= 0 && possibleColumn >= 0) && !(possibleRow == row && possibleColumn == column))
          myList.add(new int[]{possibleRow, possibleColumn});
      }
    aliveNeighbours= myList;
  }
}
