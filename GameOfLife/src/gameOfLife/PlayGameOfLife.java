package gameOfLife;

import java.util.ArrayList;
import java.util.Scanner;

import static gameOfLife.GameOfLife.*;
import static java.lang.Integer.parseInt;

public class PlayGameOfLife {

  // Get input from user and play Game Of Life

  private int size;

  public static void main(String[] args) {

    PlayGameOfLife eachGame=new PlayGameOfLife();
    GameOfLife newGame = startGameOfLife(eachGame.getInput(),eachGame.size);
    ArrayList<int []> nextGeneration=newGame.computeGeneration();
    for (int eachIndices[]:nextGeneration){
      System.out.println(eachIndices[0]+","+eachIndices[1]);
    }
  }

  private ArrayList<int[]> getInput() {

    Scanner scan = new Scanner(System.in);
    int numberOfAlive = parseInt(scan.nextLine());
    ArrayList<int[]> aliveIndices = new ArrayList<>();
    for (int forEachLine = 0; forEachLine < numberOfAlive; forEachLine++)
    {
      int[] aliveCoordinates = getInputInSpecificFormat(scan);
      size=Math.max(size,Math.max(aliveCoordinates[0],aliveCoordinates[1]));
      aliveIndices.add(aliveCoordinates);
    }
    size++;
    return aliveIndices;
  }

  private int[] getInputInSpecificFormat(Scanner scan) {

    String scannedLine[] = scan.nextLine().split(",");
    int []aliveCoordinates ;
    try {
      aliveCoordinates=new int[]{parseInt(scannedLine[0]), parseInt(scannedLine[1])};
    }
    catch (Exception e){
      System.out.println("Expected Format : X,Y");
      scannedLine=scan.nextLine().split(",");
      aliveCoordinates=new int[]{parseInt(scannedLine[0]),parseInt(scannedLine[1])};
    }
    return aliveCoordinates;
  }
}