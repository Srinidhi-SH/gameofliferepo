package gameOfLife;

import java.util.ArrayList;

import static gameOfLife.StateOfCell.dead;
import static gameOfLife.StateOfCell.live;

public class GameOfLife {

  // Create nextGeneration board and compute next generation in nextGeneration tick

  private ArrayList<Cell> cells;
  private ArrayList<int []> nextGeneration =new ArrayList<>();

  public GameOfLife(ArrayList<Cell> cell) {

    this.cells = cell;
  }

  public ArrayList<int []> computeGeneration() {

    for (Cell currentCell : cells) {
      currentCell.findNeighbours();
      currentCell.findAliveNeighbours(cells);
      currentCell.computeFinalGeneration(nextGeneration);
    }
    return nextGeneration;
  }

  public static GameOfLife startGameOfLife(ArrayList<int[]> aliveIndices, int size) {

    ArrayList<Cell> boardForGame = new ArrayList<>();
    for (int row = 0; row < size; row++) {
      for (int column = 0; column < size; column++) {
        if (contains(aliveIndices,new int[]{row, column})) {
          boardForGame.add(new Cell(row, column, live));
          continue;
        }
        boardForGame.add(new Cell(row,column,dead));
        }
      }
    return new GameOfLife(boardForGame);
  }

  private static boolean contains(ArrayList<int []> aliveIndices,int []index){

    for (int []eachIndices:aliveIndices){
      if (eachIndices[0] == index[0] && eachIndices[1] == index[1]) return true;
    }
    return false;
  }
}


