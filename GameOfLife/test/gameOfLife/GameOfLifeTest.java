package gameOfLife;

import org.junit.Test;

import java.util.ArrayList;

import static gameOfLife.StateOfCell.dead;
import static gameOfLife.StateOfCell.live;

public class GameOfLifeTest {


  @Test
  public void getCreateNewGame() {

    ArrayList<Cell> myBoard = new ArrayList<>();
    myBoard.add(new Cell(0, 0, dead));
    myBoard.add(new Cell(0, 1, dead));
    myBoard.add(new Cell(1, 0, live));
    myBoard.add(new Cell(1, 1, live));
    new GameOfLife(myBoard);
  }
/*

  @Test
  public void createGameAndTestCaseOne() {

    ArrayList<int[]> input = new ArrayList<>();
    input.add(new int[]{1, 2});
    input.add(new int[]{2, 2});
    input.add(new int[]{1, 1});
    input.add(new int[]{2, 1});
    ArrayList<int[]> expectedOutput = new ArrayList<>();
    input.add(new int[]{1, 2});
    input.add(new int[]{1, 1});
    input.add(new int[]{2, 1});
    input.add(new int[]{2, 2});
    GameOfLife game = GameOfLife.startGameOfLife(input,3);
    Assert.assertEquals(expectedOutput, game.computeGeneration());
  }

    @Test
    public void createGameAndTestCaseTwo(){

      ArrayList<int[]> input = new ArrayList<>();
      input.add(new int[]{1, 1});
      input.add(new int[]{1, 2});
      input.add(new int[]{1, 3});
      ArrayList<int[]> expectedOutput = new ArrayList<>();
      input.add(new int[]{1, 1});
      input.add(new int[]{1, 1});
      input.add(new int[]{1, 1});
      GameOfLife game = GameOfLife.startGameOfLife(input, 3);
      Assert.assertEquals(expectedOutput, game.computeGeneration());

    }

    @Test
    public void createGameAndTestCaseThree(){

      ArrayList<int[]> input = new ArrayList<>();
      input.add(new int[]{0, 1});
      input.add(new int[]{0, 2});
      input.add(new int[]{1, 0});
      input.add(new int[]{1, 2});
      input.add(new int[]{2, 1});
      ArrayList<int[]> expectedOutput = new ArrayList<>();
      input.add(new int[]{0, 1});
      input.add(new int[]{0, 2});
      input.add(new int[]{1, 0});
      input.add(new int[]{1, 2});
      input.add(new int[]{2, 1});
      GameOfLife game = GameOfLife.startGameOfLife(input, 3);
      Assert.assertEquals(expectedOutput, game.computeGeneration());
    }

    @Test
    public void createGameAndTestCaseFour(){

      ArrayList<int[]> input = new ArrayList<>();
      input.add(new int[]{1, 1});
      input.add(new int[]{1, 2});
      input.add(new int[]{1, 3});
      input.add(new int[]{2, 2});
      input.add(new int[]{2, 3});
      input.add(new int[]{2, 4});
      ArrayList<int[]> expectedOutput = new ArrayList<>();
      input.add(new int[]{0, 2});
      input.add(new int[]{1, 1});
      input.add(new int[]{1, 4});
      input.add(new int[]{2, 1});
      input.add(new int[]{2, 4});
      input.add(new int[]{3, 3});
      GameOfLife game = GameOfLife.startGameOfLife(input, 5);
      Assert.assertEquals(expectedOutput, game.computeGeneration());
    }
*/
}

